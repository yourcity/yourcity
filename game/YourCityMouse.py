#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is part of YourCity
#  Copyright (C) 2014 Moritz Strohm <ncc1988@posteo.de>

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


from fife import fife
from fife.extensions import pychan



class YourCityMouseListener(fife.IMouseListener):
  
  def __init__(self, application):
    self._engine = application.engine
    self._application = application
    self._eventmanager = self._engine.getEventManager()
    
    fife.IMouseListener.__init__(self)
    
  #handle mouse events:
    
  def mousePressed(self, event):
    if event.isConsumedByWidgets(): #if the event is handled by a pychan widget: do nothing
      return

    #determine where the mouse was clicked and let the main app handle the click
    self._application.handleMousePress(
      fife.ScreenPoint(event.getX(), event.getY())
      )
  
  def mouseReleased(self, event):
    pass #to be implemented!
  
  def mouseMoved(self, event):
    pass #to be implemented!
  
  def mouseEntered(self, event):
    pass #to be implemented!
  
  def mouseExited(self, event):
    pass #to be implemented!
  
  def mouseClicked(self, event):
    pass #to be implemented!

  def mouseWheelMovedUp(self, event):
    pass #to be implemented!
  
  def mouseWheelMovedDown(self, event):
    pass #to be implemented!
              
  def mouseDragged(self, event):
    pass #to be implemented!
