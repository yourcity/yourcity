#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is part of YourCity
#  Copyright (C) 2014 Moritz Strohm <ncc1988@posteo.de>

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


from fife.extensions import pychan

class SimpleDialog(object):
  
  def __init__(self, confirmFunction, abortFunction, titleText, message, confirmText, abortText):
    self._widget = pychan.loadXML('gui/SimpleDialog.xml')
    self._widget.findChild(name="DialogTitle").text = unicode(titleText)
    self._widget.findChild(name="DialogMessage").text = unicode(message)
    self._widget.findChild(name="ConfirmButton").text = unicode(confirmText)
    self._widget.findChild(name="AbortButton").text = unicode(abortText)
    
    self._confirmFunction = confirmFunction
    self._abortFunction = abortFunction
    
    self._widget.mapEvents({
      'ConfirmButton': self.onConfirm,
      'AbortButton': self.onAbort,
      })
  
  def onConfirm(self): #when the user clicks on yes (or save)
    print "onConfirm called!"
    self._widget.hide()
    self._confirmFunction()
    
  def onAbort(self): #when the user clicks on abort (or no)
    print "onAbort called!"
    self._widget.hide()
    self._abortFunction()
  
  def show(self): #show the dialog
    self._widget.show()
    
  def hide(self): #hide the dialog
    self._widget.hide()